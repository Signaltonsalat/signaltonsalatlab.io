---
layout: page
title: About
---

Personal dev blog for whenever I feel like I have something to report
development-wise. Don't expect much. I code a lot, but not everything is fun
enough to warrant a blog post about it.

If you want to reach out, you can find me on other sites:

* Twitter: [@Signaltonsalat](https://twitter.com/Signaltonsalat)
* Steam: [Signaltonsalat](https://steamcommunity.com/id/signaltonsalat)
* GitLab: [Signaltonsalat](https://gitlab.com/Signaltonsalat)
* Reddit: [/u/Signaltonsalat](https://www.reddit.com/user/Signaltonsalat/)

Cheers
